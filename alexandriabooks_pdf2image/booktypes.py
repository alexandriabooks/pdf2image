import typing
from io import BytesIO
import asyncio

import pdf2image.pdf2image
from pdf2image import convert_from_path

from alexandriabooks_core import accessors, model
from alexandriabooks_core.services import BookParserService


async def async_convert_from_path(*args, **kwargs):
    def _do_convert():
        return convert_from_path(
            *args,
            **kwargs
        )

    return await asyncio.get_running_loop().run_in_executor(None, _do_convert)


class PDFArchiveBookAccessor(accessors.ArchiveBookAccessor):
    def __init__(self, accessor: accessors.FileAccessor):
        super(PDFArchiveBookAccessor, self).__init__(accessor)

    async def get_file_listing(self):
        page_count = pdf2image.pdf2image.pdfinfo_from_path(self.get_local_path())[
            "Pages"
        ]
        return [f"page{page_no}.jpg" for page_no in range(1, page_count)]

    async def get_file_stream(self, path):
        page_number = int(path[4:-4])
        images = await async_convert_from_path(
            self.accessor.get_local_path(),
            first_page=page_number,
            last_page=page_number,
            fmt="jpg",
        )

        if not images:
            return None

        image_stream = BytesIO()
        images[0].save(image_stream, format="JPEG")
        image_stream.seek(0)

        return image_stream

    async def get_file_streams(self):
        images = await async_convert_from_path(
            self.accessor.get_local_path(),
            fmt="jpg",
        )

        def image_to_stream(image):
            image_stream = BytesIO()
            image.save(image_stream, format="JPEG")
            image_stream.seek(0)

            return image_stream

        return [
            (f"page{page_no}.jpg", image_to_stream(image))
            for page_no, image in zip(range(1, len(images) + 1), images)
        ]


class PDFArchiveBookParserService(BookParserService):
    def __init__(self, **kwargs):
        pass

    def __str__(self):
        return "PDF to Image Book Parser"

    async def can_parse(self, accessor: accessors.FileAccessor):
        source_type = model.SourceType.find_source_type(accessor.get_filename())
        return source_type in [model.SourceType.PDF]

    async def parse_book(self, accessor: accessors.FileAccessor):
        if model.SourceType.find_source_type(
            accessor.get_filename()
        ) == model.SourceType.PDF:
            metadata = {"source_type": model.SourceType.PDF}
            return metadata
        return None

    async def enhance_accessor(
        self,
        accessor: model.FileAccessor
    ) -> typing.List[model.FileAccessor]:
        """
        Attempts to find a more specific accessor to aid processing the book.
        :param accessor: The accessor to read the book.
        :return: A list of enhanced accessors.
        """
        if model.SourceType.find_source_type(
            accessor.get_filename()
        ) == model.SourceType.PDF:
            return [PDFArchiveBookAccessor(accessor)]
        return []
