import typing
import logging

logger = logging.getLogger(__name__)
ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}

try:
    from .booktypes import PDFArchiveBookParserService
    import pdf2image.pdf2image

    pdf2image.pdf2image._get_poppler_version("pdfinfo")

    ALEXANDRIA_PLUGINS["BookParserService"] = [PDFArchiveBookParserService]
except (ImportError, OSError, FileNotFoundError) as e:
    logger.debug(
        "Skipping loading PDF2Image plugin, please ensure poppler-utils is installed",
        exc_info=e
    )
